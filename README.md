# StillPhotoSeriesHelper

Scripts for automation of taking photo series with fswebcam.

fswebcam is a useful tool to take images trogh a video device on linux and very
useful as it is. More information can be found on
https://github.com/fsphil/fswebcam and in the manual page.

For repeatedly scanning dia frames, negatives  oreven  making animations with 
the help of static images where the scenery change a little at a time it is
however cumbersome to change the file name from image to image. The scripts
help automate the procedure so that the user just need to hit enter to make a
new image in the series. 

## Usage

nextseries.sh creates a new folder for an image series named from S01 to
S99, which ever number is the next in line. The scripts places the series
folders in the Documents folder.  Subsequently the script starts scan.sh in the
newly created folder.

scan.sh then checks if the video device (/dev/video0) exists, and that
fswebcam is installed. Subsequently the script proceeds to prompt the user to
take a photo confirm the next image that is automatically named
S[01-99]_B[01-99].jpeg, which ever is next in the current series. If the user
just hits enter the image is saved, but if the user enters a 'n' the scanning
stops.

If a file already exists the user is prompted to confirm overwriting the old
file  with a 'y'.

## Installation

Installation is done by cloning this repository and copying the *.sh files in
the cloned directory into ~/bin/ or another directory on the path. After this
just execute ~/bin/nextseries.sh or where every you placed the script.

The scripts can be modified in terms of folder structure, fswebcam arguments 
and nameing convention and whatever... The scripts are implemented in bash,
although uses also tools like sed and which.

fswebcam has lots of tools that can be used to tweak the settings of the image
capture. A more evolved version could be constructed to enable preview, rotation
and tweaking image quality settings can be envisioned, but is not there at
present.

## Known issues

The system has not been very thoroughly tested yet. For the time beeing im busy
getting the DIA:s scanned... I also only have one lousy scanner (SilverCrest
from Lidl). It will be interesting to see if it works with an other.

The scanner I happen to have have a warm up issue that makes the first image
look quite bleached. I circumvent this by manually issuing a few scans of an
empty scanner to /dev/null. 

Also, although I know how to use a spell checker, I have not got around to
it... Sorry... 

## About these scripts

I got a large box full of DIA format frames and pictures. Today you do not do
very much with DIA images. They are cumbersome to view as you normally do 
not have a projector or a screen at hand and setting it up takes time as well as
does putting them away after. The DIA images are as well subject to bleaching
from storage conditions (heat/cold/dampness/physical wear) and based on their
conditions today, roughly 30 years after the camera took the picture, it looks
like they eill not last for ever. A digital form is preferable as the images can
be shared and accessed more easily. The long term storage asepct may not be any better, but at least the rate of degradation should not be an issue.

![Box full of smaller boxes, each containing 2 * 50 racks for DIA frames, almost at capacity.](./images/box.jpg "Boxes of DIA frames")

So, why not scan the DIA frames and share them for the larger family to
view i thought... Well it is not that easy. I happened to own a DIA scanner,
bought on wim a few years ago. Never opened the box until now. As time have
passed since  the scanner was bought the versions of operating systems and 
even the computer architectures have evolved since then. The driver that came
with the scanner does not work under a new version of windows, nor does the 
mac driver work on a up to date version of MacOS. And the driver does 
definitely not work on a M1 or M2 architecture Mac. 

So... Sofar that wasted a day or more. On the other hand I had a few 
RaspberryPi devices available and I can run virtual Linux machines for
testing. But there was no driver. It turns out that you do not need one. Linux
happens to recognize the scanner as a video camera.  fswebcam can be found 
at least for debian and is able to take images from a video camera and store to
a file.

The scanner connect to the computer trough a USB cable. For virtual linux hosts
the virtual host needs to assign the USB device to the virtual machine guest,
but at least on my Mac this works in UTM without problems.

Scanning manually turned out to be very time consuming and error porone, so I
set out to automate the file naming, and here we are... Scanning 20 boxes of DIA
frames will take time, and I would not recommend it  to anyone, but now it is
possible, even on  a cheap linux. 

![Scanner, computer and screen with command line in use ](./images/setup.jpg "Running the scanning")

There are still problems that can't be solved with a script. It is not easy to
know what is on the image. As our memory fade and the person taking the DIA
photo is gone there is not much to do about this problem. Some DIA frames have
written information on them, and a second camera taking a picture of the frame
could be useful, but as the text is not easily readable this does not solve the
problem entirely. 

## Further ideas

Perhaps these scripts and this setup could be used to create clay (or other)
animations like https://en.wikipedia.org/wiki/Bruce_Bickford_(animator) used in
his films, by simply using a fixed normal camera and a chep computer in front of
the scene of the animation.

## License

The script can be distributed and used under the GNU General Public License.

    VERSION: 1.0.0 - See https://codeberg.org/aaspnas/StillPhotoSeriesHelper for commit history
	
    License: GPL-3 - See license file in the distribution, or on Codeberg
	
    SPDX-License-Identifier: GPL-3.0-or-later
