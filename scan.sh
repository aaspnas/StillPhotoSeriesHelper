#!/bin/bash

# Script to automate scanning or taking a picture with default camera
# Uses fswebcam (apt install fswebcam) to capture image
# Prepends the filename with the directory, and names the file with
# a sequence number from 01 to 99 to keep directory listing ordered.
#
#     SPDX-License-Identifier: GPL-3.0-or-later
#
# Version 1.0.0 2023-04-05 - Aaspnas@codeberg
#


CAMERADEVICE="/dev/video0"
CAMERARESOLUTION="2592x1680"
FRAMENUMBER=6
FRAMESKIP=2
CAMERADELAY=1
CAMERAINDEX=0

## Check that we have fswebcam installed
FCAM=$(which fswebcam)
if [  -z $FCAM ]
then   
    echo "Please install fswebcam and make sure it exists on the path"
    exit
fi

## Check that camera is attached to /dev/video0 index 0 
if [  ! -r $CAMERADEVICE   ]
then
    echo "Camera $CAMERADEVICE is not attached. Please connect the camera to the computer."
    exit
fi

FSCAMARGS="-r $CAMERARESOLUTION -d $CAMERADEVICE -F $FRAMENUMBER -S $FRAMESKIP -D $CAMERADELAY -i $CAMERAINDEX"

if [ $FILENUMBER ]
then
    echo "File number is $FILENUMBER" 

else
    if [ $1 ]
    then
	FILENUMBER=$1
    else
	FILENUMBER=1
    fi
fi

export SERCODE=$(pwd | sed -e "s/.*\///g")

if [[ "$SERCODE" =~ ^S[0-9]{2}$ ]]
then
    SCANNEXT=1
else
    SCANNEXT=0
fi


while
    [ "$SCANNEXT" == "1" ] && [ $FILENUMBER -lt  100 ]
do
    export FILECODE=$(printf "B%02d" $((FILENUMBER)))
    FILENAME="${SERCODE}_${FILECODE}.jpeg"
    read -p "Scan $FILENAME [y|n]? " cont
    if [[ $cont == "n" ]]
    then
#	echo $cont
	SCANNEXT=0
    else
	if [ -f "$FILENAME"  ]
	then
	    read -p "Overwrite? [y|n]" overwrite
	    if [[ $overwrite == "y" ]]
	    then
		fswebcam $FSCAMARGS $FILENAME
	    else
		echo "trying next file name..."
	    fi
	else
	    fswebcam $FSCAMARGS $FILENAME
	fi	       
       	((FILENUMBER++))
	export FILENUMBER
    fi
done

