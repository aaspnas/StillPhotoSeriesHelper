#!/bin/bash

# Script to set up directories under a base dir for scanning.
# Creates next directory in sequence 00 to 99 based on what exists
#
#     SPDX-License-Identifier: GPL-3.0-or-later
#
# Version 1.0.0 2023-04-05 - Aaspnas@codeberg
#

BASEDIR=~/Documents
cd $BASEDIR
declare -i SERIES=0
SERIES=`ls -1 -d S* | tail -1 | sed -e "s/^S//" -e "s/^0//"` 
echo $SERIES
((SERIES++))
echo $SERIES
if [ $SERIES -lt 100 ]
then
    
    SERCODE=$(printf "S%02d" $((SERIES)));
    export SERCODE
    export SCANDIR="$BASEDIR/$SERCODE"
    mkdir $SCANDIR

    echo "Created directory $SCANDIR for scanning."
    echo "Now run '~/bin/scan.sh'"

    cd $SERCODE
    . ~/bin/scan.sh
    
else
    echo "limit reached!"
fi
